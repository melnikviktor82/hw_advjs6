// Теоретичне питання

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// JavaScript сам по собі однопотоковий, що означає те, що лише один блок коду може запускатися за один раз і виникає черга обробки завдань.
// Але якщо такі завдання, як обробка зображень, операції з файлами, створення запитів мережі та очікування відповіді -
// все це може гальмувати і бути довгим. Асинхронність дозволяє запуск потрібного нам завдання у фоновому режимі,
// дозволяючи наступному завданню запуститися без очікування завершення попереднього завдання.

// Завдання

// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:

// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

// "use strict";

function getUserGeolocation(btnId) {
  const btn = document.querySelector(`#${btnId}`);

  btn.addEventListener("click", async function fn() {
    try {
      const loader = document.createElement("p");
      loader.innerText = "Please, wait...";
      btn.after(loader);

      const responseIP = await fetch("https://api.ipify.org/?format=json");
      const userIP = await responseIP.json();

      const responseGeoLoc = await fetch(
        `http://ip-api.com/json/${userIP.ip}?fields=continent,country,regionName,city,district`
      );
      const geoLocData = await responseGeoLoc.json();

      const { continent, country, regionName, city, district } = geoLocData;

      const userInfo = Object.entries({
        continent,
        country,
        regionName,
        city,
        district,
      }).map(([key, value]) => {
        return ` ${key.replace(
          key[0],
          key[0].toUpperCase()
        )}: "${value.bold()}"<br><br>`;
      });

      const userInfoList = document.createElement("p");
      userInfoList.innerHTML = userInfo.join("");
      loader.remove();
      btn.after(userInfoList);
      btn.removeEventListener("click", fn);
    } catch (error) {
      alert(error.message);
    }
  });
  return btn;
}

getUserGeolocation("findGeolocationBtn");
